class FollowsController < ApplicationController
	before_action :signed_in_user
  	before_action :set_follow, only: [:show, :edit, :update, :destroy]

# GET /memberships
  # GET /memberships.json
  def index
    @follows = Follow.all
  end

  # GET /memberships/1
  # GET /memberships/1.json
  def show
  end

  # GET /memberships/new
  def new
    @follow = Follow.new
  end

  # GET /memberships/1/edit
  def edit
  end


  def create
    #Probably really bad #fixme
    #@group = Group.find(params[:Membership][:group_id])
    @bounty = Bounty.find(params["follow"]["bounty_id"])
    current_user.follow!(@bounty)
    respond_to do |format|
      format.html { redirect_to @bounty }
      format.js
    end
  end



  # POST /memberships
  # POST /memberships.json
  #def create
  #  @membership = Membership.new(membership_params)
  #
  #  respond_to do |format|
  #    if @membership.save
  #      format.html { redirect_to @membership, notice: 'Membership was successfully created.' }
  #      format.json { render action: 'show', status: :created, location: @membership }
  #    else
  #      format.html { render action: 'new' }
  #      format.json { render json: @membership.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end

  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    respond_to do |format|
      if @follow.update(follow_params)
        format.html { redirect_to @follow, notice: 'Follow was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @follow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /memberships/1
  # DELETE /memberships/1.json
  #def destroy
  #  @membership.destroy
  #  respond_to do |format|
  #    format.html { redirect_to memberships_url }
  #    format.json { head :no_content }
  #  end
  #end

  def destroy
    @bounty = Follow.find(params[:id]).bounty
    current_user.unfollow!(@bounty)
    respond_to do |format|
      format.html { redirect_to current_user }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_follow
      @follow = Follow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def follow_params
      params.require(:follow).permit(:user_id, :bounty_id)
    end
end
