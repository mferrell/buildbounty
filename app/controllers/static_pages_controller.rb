class StaticPagesController < ApplicationController
  def home
    if signed_in?
      @bounty_feed_items = current_user.followed_bounties.paginate(page: params[:page])
    end
  end

  def about
  end

  def help
  end

  def discover
    #Implement function to show preferred bounties.  For now, random.
    @bounty_feed_items = Bounty.all.slice((rand()*80).round, 10)
  end

  def start
  end

end
