json.array!(@bounties) do |bounty|
  json.extract! bounty, :name, :user_id, :content
  json.url bounty_url(bounty, format: :json)
end
