json.array!(@submissions) do |submission|
  json.extract! submission, :user_id, :bounty_id, :title, :content, :upvotes, :downvotes
  json.url submission_url(submission, format: :json)
end
