class Tag < ActiveRecord::Base
	has_many :taggings
	has_many :bounties, through: :taggings
end
