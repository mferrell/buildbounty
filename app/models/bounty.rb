class Bounty < ActiveRecord::Base
		belongs_to :user, class_name: "User"
		validates :user_id, presence: true
		has_many :follows, foreign_key: "bounty_id",
                                   class_name:  "Bounty",
                                   dependent:   :destroy
        has_many :taggings
        has_many :tags, through: :taggings
        has_many :submissions

        #attr_accessible :mimage
        # This method associates the attribute ":avatar" with a file attachment
		has_attached_file :mimage, styles: {
    		tiny: '30x30#',
    		card: '200x150#',
    		medium: '300x300#'
  		}, :default_url => "/images/:style/missing.png"

        def self.tagged_with(name)
        	Tag.find_by_name!(name).bounties
        end

        def self.tag_counts
        	Tag.select("tags.*, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id")
        end

        def tag_list
        	tags.map(&:name).join(", ")
        end

        def tag_list=(names)
        	self.tags = names.split(",").map do |n|
        		Tag.where(name: n.strip).first_or_create!
        	end
        end
end
