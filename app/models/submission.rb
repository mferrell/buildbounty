class Submission < ActiveRecord::Base
	belongs_to :user, class_name: "User"
	belongs_to :bounty, class_name: "Bounty"

	has_attached_file :simage, styles: {
			tiny:  '30x30',
    		thumb: '100x100>',
    		square: '200x200#',
    		medium: '300x300>'
  		}, :default_url => "/images/:style/missing.png"

end
