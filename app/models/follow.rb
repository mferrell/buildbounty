class Follow < ActiveRecord::Base
	belongs_to :bounty, class_name: "Bounty"
	validates :user_id, presence: true
	validates :bounty_id, presence: true
end
