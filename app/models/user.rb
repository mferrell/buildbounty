class User < ActiveRecord::Base
  has_many :bounties, dependent: :destroy
  has_many :follows, foreign_key: "user_id", dependent: :destroy	
  has_many :followed_bounties, through: :follows, source: :bounty

  has_secure_password
  before_save { self.email = email.downcase }
  before_create :create_remember_token
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }


  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end
  
  def following?(bounty)
    follows.find_by(bounty_id: bounty.id)
  end

  def follow!(bounty)
    follows.create!(bounty_id: bounty.id)
  end

  def unfollow!(bounty)
    follows.find_by(bounty_id: bounty.id).destroy!
  end


  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
