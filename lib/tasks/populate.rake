namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'populator'
    require 'faker'
    
    
    [User, Bounty].each(&:delete_all)

    User.create(name: "Matt", email: "mattferrell2@gmail.com", password: "gEAHMr17", password_confirmation: "gEAHMr17")
    999.times do |n|
      User.create(name: Faker::Name.name, 
        email: Faker::Internet.email, 
        password: "password", 
        password_confirmation: "password")  
    end

   100.times do |n|
      Bounty.create(name: Populator.words(1..3).titleize,
        user_id:(rand*98).round+1,
        content: Populator.sentences(2..10))
    end

    1000.times do |n|
      Follow.create(user_id:(rand*998).round+1,
        bounty_id:(rand*98).round+1)
    end
  end
end