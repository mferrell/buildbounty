class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.integer :user_id
      t.integer :bounty_id
      t.string :title
      t.string :content
      t.integer :upvotes
      t.integer :downvotes

      t.timestamps
    end
  end
end
