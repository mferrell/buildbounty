class AddMainImageToBounties < ActiveRecord::Migration
  def self.up
    add_attachment :bounties, :mimage
  end

  def self.down
    remove_attachment :bounties, :mimage
  end
end
