class CreateBounties < ActiveRecord::Migration
  def change
    create_table :bounties do |t|
      t.string :name
      t.integer :user_id
      t.string :content

      t.timestamps
    end
  end
end
