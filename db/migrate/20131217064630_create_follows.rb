class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows do |t|
      t.string :user_id
      t.string :integer
      t.integer :bounty_id

      t.timestamps
    end
  end
end
