class AddImageToSubmissions < ActiveRecord::Migration
   def self.up
    add_attachment :submissions, :simage
  end

  def self.down
    remove_attachment :submissions, :simage
  end
end
